FROM ghcr.io/mlflow/mlflow:v2.10.0

USER root

# Install dependencies
RUN pip install boto3 psycopg2-binary

# Create and populate the credentials file using environment variables
ARG AWS_ACCESS_KEY_ID
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ARG POSTGRES_USER
ARG POSTGRES_PASSWORD
ENV MLFLOW_BACKEND_STORE_URI=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@mlflow-postgres:5432/mlflow
ENV MLFLOW_S3_ENDPOINT_URL=http://mlflow-minio:9000

# scenario 4
ENV MLFLOW_DEFAULT_ARTIFACT_ROOT=s3://mlflow

CMD [ \
    "mlflow", "server", \
    "--no-serve-artifacts", \
    "--host", "0.0.0.0", \
    "--workers", "20" \
]
