# MLFLOW FOR POOR MAN

This repository contains scripts and configuration files for the series of articles published on Medium with the same name.

* [Part I](https://wervlad.medium.com/mlflow-for-poor-man-part-i-b955bfe35df2) covers installation of MLflow and all necessary services for its operation on Raspberry Pi.
* [Part II](https://wervlad.medium.com/mlflow-for-poor-man-part-ii-3f4adb9c6f86) tells how to configure authorization and traffic encryption for secure operation on the Internet.
* [Part III](https://wervlad.medium.com/mlflow-for-poor-man-part-iii-4f57a530942f) deals with configuring OpenVPN as reverse proxy for Raspberry Pi server behind a private IP address and hardening security with fail2ban.

## Usage
Install Docker and Docker Compose. Then run
```bash
make all
```

Log in to the MinIO web interface and create an `Access Key` and `Secret Key`. Please paste the values that you specified in the `.env` file because MLflow will use them for authentication in MinIO.

To add a user to Nginx basic auth, type:
```bash
make add-user
```

And to delete her:
```bash
make del-user
```

To start previously created containers, type
```bash
make run-all
```

To launch Docker containers at system startup (for systemd init system)
```bash
make add-to-startup
```

And to remove them from startup:
```bash
make del-from-startup:
```

To clean up resources created by this scripts:
```bash
make clean
```

To get help on commands:
```bash
make help
```

If you found this repository useful, please give it a star! ⭐

It will motivate me to create new content and provide information on which topics and projects are more in demand.
