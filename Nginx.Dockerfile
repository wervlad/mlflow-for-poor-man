FROM nginx

USER root

# install apache2-utils, tzdata and certbot
RUN apt-get update && apt-get install -y apache2-utils tzdata certbot

# Set the timezone to Asia/Tbilisi
# Pick yours from the output of the command `timedatectl list-timezones`
ARG TIMEZONE
ENV TZ=$TIMEZONE

# Configure the timezone
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
