include .env
export

## Create and run all necessary resources
all: create-dirs
	docker compose up -d

## Delete all docker resources created by this scripts
clean:
	docker compose down
	-docker image rm mlflow
	-docker image rm mlflow-nginx

# Create directory structure
create-dirs:
	sudo mkdir -p /data/bucket/$$MLFLOW_BUCKET
	sudo cp -r nginx /data/

## Run all docker containers
run-all: run-postgres-container run-minio-container run-mlflow-container \
	run-nginx-container

## Stop docker containers
stop-all: stop-nginx-container stop-mlflow-container stop-minio-container \
	stop-postgres-container

# Run Postgres container
run-postgres-container:
	docker start mlflow-postgres

# Stop Postgres container
stop-postgres-container:
	-docker stop mlflow-postgres

# Run MinIO container
run-minio-container:
	docker start mlflow-minio

# Stop MinIO container
stop-minio-container:
	-docker stop mlflow-minio

# Run MLflow container and containers required for its operation
run-mlflow-container: run-postgres-container run-minio-container
	docker start mlflow-server

# Stop MLflow container
stop-mlflow-container:
	-docker stop mlflow-server

# Run Nginx container
run-nginx-container:
	docker start mlflow-nginx

# Run Nginx container
stop-nginx-container:
	-docker stop mlflow-nginx

## add user to Nginx basic auth
add-user:
	@docker exec -it mlflow-nginx bash -ic \
		'read -p "Enter username: " NEW_USER; \
		htpasswd /data/nginx/.htpasswd $$NEW_USER'

## delete user from Nginx basic auth
del-user:
	@docker exec -it mlflow-nginx bash -ic \
		'read -p "Enter username: " NEW_USER; \
		htpasswd -D /data/nginx/.htpasswd $$NEW_USER'

## launch Docker containers at system startup
add-to-startup:
	sudo cp systemd/mlflow-postgres.service /etc/systemd/system/mlflow-postgres.service
	sudo cp systemd/mlflow-minio.service /etc/systemd/system/mlflow-minio.service
	sudo cp systemd/mlflow-nginx.service /etc/systemd/system/mlflow-nginx.service
	sudo cp systemd/mlflow-server.service /etc/systemd/system/mlflow-server.service
	sudo systemctl enable mlflow-postgres
	sudo systemctl enable mlflow-minio
	sudo systemctl enable mlflow-nginx
	sudo systemctl enable mlflow-server

## remove Docker containers from system startup
del-from-startup:
	sudo systemctl disable mlflow-postgres
	sudo systemctl disable mlflow-minio
	sudo systemctl disable mlflow-nginx
	sudo systemctl disable mlflow-server
	sudo rm /etc/systemd/system/mlflow-postgres.service
	sudo rm /etc/systemd/system/mlflow-minio.service
	sudo rm /etc/systemd/system/mlflow-nginx.service
	sudo rm /etc/systemd/system/mlflow-server.service


#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')

